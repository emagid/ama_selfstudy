<?php
/**
 * Template Name: Services Template
 *
 */

get_header();
?>
    <div class="hero section section_default" id="splash">
        <div class="wrapper">
        <div class="hero_brand">
            <div class="wrapper">
        <div class="content">
            <p><?php the_title(); ?></p>
            <h2><?php the_field('tagline'); ?></h2>
            
        </div>
            </div>
        </div>
            
            <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mobile_app.png" style="top:50%;right:9%;">
                
                <div class="orange_side">
                    
                </div>
            </div>
        </div>
    
</div>


<section>
    <div class="our_services_section">
        <div class="wrapper">
            
            <div class="service_circles">
                <div class="inner_circle">
                    <h5><?php the_field('section_header_1'); ?></h5>
                    <p><?php the_field('section_content_1'); ?></p>
                </div>
                
                <div class="inner_circle">
                    <h5><?php the_field('section_header_2'); ?></h5>
                    <p><?php the_field('section_content_2'); ?></p>
                </div>
            </div>
            
            <div class="service_circles">
                <div class="inner_circle">
                    <h5><?php the_field('section_header_3'); ?></h5>
                    <p><?php the_field('section_content_3'); ?></p>
                </div>
                            
                <div class="inner_circle">
                    <h5><?php the_field('section_header_4'); ?></h5>
                    <p><?php the_field('section_content_4'); ?></p>
                </div>
                            
                <div class="inner_circle">
                    <h5><?php the_field('section_header_5'); ?></h5>
                    <p><?php the_field('section_content_5'); ?></p>
                </div>
                
            </div>

        </div>
    </div>

</section>

<!--
    <section class="section_three_home" style="background-image:url(<?php the_field('feature_background'); ?>);">
        <div class="content">
            <div class="spotlight">
                <h4 style="text-transform:none;font-size:28px;"><?php the_field('feature_project'); ?></h4>
                <p><?php the_field('feature_text'); ?></p>
                <div class="cta">
                    <a href="">
                        <button>See Case Study <img src="http://emagid.test/wp-content/themes/emagid/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
            
        </div>
        
    </section>
-->



<section class="service_flex">
    <div class="flex" id="website">
        <div class="flex_img flex_650" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/poster_dji.jpg)">
            <a href="/portfolio/dji-nyc/">
                <div class="flex_overlay">
                    <p>DJI NYC</p>
                    <h5>Explore The World <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                </div> 
            </a>
        </div>
        
        <div>
            <div class="flex_img flex_375" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/poster_cliff.jpg)">
                <a href="/portfolio/cliff-young/">
                    <div class="flex_overlay">
                        <p>Cliff Young LTD.</p>
                        <h5>Furniture Company with Style. <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                    </div>  
                </a>
            </div>
            <div class="flex_img flex_275" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/projects_emagid.jpg)">
                <a href="/projects/">
                    <div class="flex_overlay flex_overlay_dark">
                        <p>View All Projects</p>
                        <h6>See more examples of our branding, design and  development in action<br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h6>
                    </div>  
                </a>
            </div>
        </div>
    </div>
</section>







<!--


<section class="default_content more_work">
    
        <div class="image_grid">
      <//?php
        $args = array(
        'post_type' => 'portfolio',
            'posts_per_page' => 3
        );
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
    

        <a href="<?php the_permalink(); ?>">
            <div class="related">
                <div class="wrapper">
            <img src="<?php the_field('feature_image'); ?>">
            <img class="more_work_arrow" src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png">
                    </div>
                </div>
        </a>
    
    

    <//?php
        }
            }
        else {
        echo 'No Projects Found';
        }
    ?> 
            </div>
</section>
-->

        <div class="hero section" id="section3">
                <section class="section_touch " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png);height:auto;padding-bottom:100px;">
        <div class="content">
            <h4>TELL US MORE ABOUT YOUR PROJECT</h4>
            <div class="cta">
                <a href="/request-a-quote/">
                    <button>Let's Talk <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                </a>
            </div>
        </div>

    </section>

    </div>


<script>
$(".icon_triggers .icon img").click(function(){
    $(".icon_triggers .icon img").removeClass('lightup');
    $(this).addClass('lightup');
});
</script>


<?php
get_footer();
