<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>
    <section class="section_default hero_portfolio" style="background-position:bottom;">
        <div class="hero_portfolio_wrapper">
        <div class="content">
            <p><span><?php the_title(); ?></span></p>
            <h2><?php the_field('tagline'); ?></h2>
<!--            <p class="intro">Custom, open source & SEO-friendly web development for projects of any size, never outsourced. Enterprise Solutions provided by eMagid deliver on the promise built-in to their own name – we work as a true partner alongside your business to develop a solution singular to the needs of your enterprise.</p>-->
        </div>
        <div class="feat_image">
            <img src="<?php the_field('feature_image'); ?>">
        </div>
        </div>
    </section>

<section class="screenshot">
    <div class="wrapper">
        <img src="<?php the_field('screenshot'); ?>">
    </div>
</section>

<section>
    <div class="phone_view" style="background-image:url(<?php the_field('mobile_view'); ?>)">
    </div>
</section>

<section class="service_flex">
    
    <div class="flex project_flex">
        <div class="flex_img mobile_only" style="background-image:url(<?php the_field('detail_image_1'); ?>)">
        </div> 
        <div class="flex_content">

            <div class="wrapper">
                <h5 class="font_oj"><?php the_field('content_title_1'); ?></h5>
                <p><?php the_field('content_text_1'); ?></p>
            </div>
        </div>
        <div class="flex_img desktop_only" style="background-image:url(<?php the_field('detail_image_1'); ?>)">
        </div>
    </div>
    
    <div class="flex project_flex" >
        <div class="flex_img" style="background-image:url(<?php the_field('detail_image_2'); ?>)">
        </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h5 class="font_oj"><?php the_field('content_title_2'); ?></h5>
                <p><?php the_field('content_text_2'); ?></p>
            </div>
        </div>
    </div>
    
</section>


    <section class="section_touch inner " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png)">
        <div class="content">
            <h4>Tell Us More About Your Project</h4>
            <div class="cta">
                <a href="/request-a-quote/">
                    <button>Get Started <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                </a>
            </div>
        </div>
        
    </section>

<section class="service_flex">
    <div class="flex" id="website">
        <div class="flex_img flex_650" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/poster_dji.jpg)">
            <a href="/portfolio/dji-nyc/">
                <div class="flex_overlay">
                    <p>DJI NYC</p>
                    <h5>Explore The World <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                </div>  
            </a>
        </div>
        
        <div>
            <div class="flex_img flex_375" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/poster_cliff.jpg)">
                <a href="/portfolio/cliff-young/">
                    <div class="flex_overlay">
                        <p>Cliff Young LTD.</p>
                        <h5>Furniture Company with Style. <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                    </div>  
                </a>
            </div>
            <div class="flex_img flex_275" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/projects_emagid.jpg)">
                <a href="/projects/">
                    <div class="flex_overlay flex_overlay_dark">
                        <p>View All Projects</p>
                        <h6>See more examples of our branding, design and  development in action<br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h6>
                    </div>  
                </a>
            </div>
        </div>

    </div>

</section>


<?php
get_footer();
