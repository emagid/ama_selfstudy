<?php
/**
 * Template Name: Hotels Template
 *
 */
get_header();
?>
<section class="inner_template">     

    <div class="nav_bar" id="sticker">
        <?php get_sidebar('location'); ?>
    </div>
    <section class="section_three_home" id="overview">
        <div class="content">
            <h2>Hotels</h2>
            <?php the_field('overview'); ?>
        </div>
    </section>

      
    <section>
        <div class="our_services_section pricing" id="rates">
            <div class="wrapper">

                
                <section class="section_five_home" id="hotels">
                    <div class="content">

                                          <?php
        //                Washington DC
                        if( $post->post_parent == 324) {
                $args = array(
                'post_type' => 'hotels',
                    'cat' => '10'
                    
                );
                            } 
        //                San Francisco
                        else if( $post->post_parent == 326 ) {
                            
                        $args = array(
                'post_type' => 'hotels',
                            'cat' => '9'
                ); 
                        }
        //                Atlanta
                        else if( $post->post_parent == 320 ) {
                            
                        $args = array(
                'post_type' => 'hotels',
                            'cat' => '8'
                ); 
                        }
        //                New York
                        else {
                            
                        $args = array(
                'post_type' => 'hotels',
                            'cat' => '7'
                ); 
                        }
                        
                       
                $products = new WP_Query( $args );
                    if( $products->have_posts() ) {
                    while( $products->have_posts() ) {
                $products->the_post();
            ?> 
                        <div class="media_box" style="opacity:1!important;transform:translateY(0)!important;">

                            <div class="header">
                                <div class="wrapper">
                                    <h5><?php the_field('name'); ?></h5>
                                    <p><?php the_field('note'); ?></p>
                                </div>
                            </div>     
                            <div class="price_chart">
                                <div class="iframe_map">
                                    <?php the_field('google_map'); ?>
                                </div>
                                <div class="hotel_info">
                                    <p><?php the_field('address'); ?></p>
                                    <p><strong><?php the_field('promo'); ?></strong></p>
                                </div> 
                                <div class="cta">
                                <a href="<?php the_field('reservation_link'); ?>">
                                    <button>Reserve</button>
                                </a>
                                </div>
                            </div>
                        </div>
            <?php
                }
                    }
                else {
                echo 'No Hotels Found';
                }
            ?> 
                 
                        


                    
                    </div>
                </section>
            </div>
        </div>
    </section>
</section> 



<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>

<script>
$(document).ready(function(){

    $(".nav_bar").find("a.scroll").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });
    
    $("#sticker").sticky({topSpacing:0});
    
    
});
</script>

<?php
get_footer();
