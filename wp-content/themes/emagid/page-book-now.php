<?php

get_header();
?>
  <section class="inner_template">     
<!--
    <div class="hero section" id="splash" style="background-image:url(<?php the_field('banner'); ?>);">
        <div class="wrapper">
                        <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                
                    <div class="inner_hero_content">
                        <div class="wrapper">
                            <h2>Offices in</h2>
            
            
                            <div class="copy">
                                <p style="width:100%;">New York, Atlanta, San Francisco & Washington, D.C.</p>
                            <div class="cta">
                                <a href="">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
      

        </div>
    
</div>
-->
<div class="nav_bar" id="sticker">
    <?php get_sidebar('location'); ?>

</div>


    <section class="section_six_home" style="background-image:url(<?php the_field('banner_image'); ?>)">
        <div class="content get_started">
            
            <div class="get_started_header">
            <h5>Complete the form below to</h5>
                <h4>Book A Meeting</h4>
            </div>
            
        </div>
        <div class="form_container">
            <div class="form_apply form_get_started">
<!--                <//?php echo do_shortcode('[contact-form-7 id="564" title="Book Now"]'); ?>-->
                
            </div>
        </div>
    </section>

<script>
$("a.go_2").click(function(event){
    event.preventDefault();
    
    $(".step_1").hide();
    $(".step_2").show();
}); 
$("img#back_1").click(function(event){
    event.preventDefault();
    
    $(".step_2").hide();
    $(".step_1").show();
}); 
$("a.go_3").click(function(event){
    event.preventDefault();
    
    $(".step_2").hide();
    $(".step_3").show();
}); 
$("img#back_2").click(function(event){
    event.preventDefault();
    
    $(".step_2").show();
    $(".step_3").hide();
}); 
$("a.go_4").click(function(event){
    event.preventDefault();
    
    $(".step_3").hide();
    $(".step_4").show();
}); 
    $("img#back_3").click(function(event){
    event.preventDefault();
    
    $(".step_3").show();
    $(".step_4").hide();
});
</script>
<?php
get_footer();
