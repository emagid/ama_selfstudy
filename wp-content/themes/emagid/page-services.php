<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>

    <div class="hero section section_default" id="splash">
        <div class="wrapper">
        <div class="hero_brand">
            <div class="wrapper">
        <div class="content">
            <p>EMAGID IS A GLOBAL DIGITAL PRODUCTION FIRM</p>
            <h2 style="font-size:36px;"><?php the_field('mission_text'); ?></h2>
<!--
            <p><?php the_title(); ?></p>
            <h2><?php the_field('subtext'); ?></h2>
-->
            
        </div>
            </div>
        </div>
            
            <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/macbook.png" style="top:50%;right:9%;">
                
                <div class="orange_side">
                    
                </div>
            </div>
        </div>
    
</div>
<!--
    <section class="section_three_home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/skyline.png)">
        <div class="content">
            <p class="font_oj"><?php the_field('mission_title'); ?></p>
            <h4 style="text-transform:none;"><?php the_field('mission_text'); ?></h4>
        </div>
        
    </section>
-->

    <section class="section_one_home section_snapshot">
        <div class="content">
            <div class="snapshot">
                <h4>The Snapshot</h4>
     
    <div class="wrapper cards">
          <div class="container">
    <div class="card">
      <div class="front">
          <div class="box">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/app_icon.png">
                <p><?php the_field('snapshot_1_title'); ?></p>
            </div>
        </div>
      <div class="back">
          <div class="box">
            <p><?php the_field('snapshot_1_text'); ?></p>
                <div class="cta">
                    <a href="/services/web-development/">
                        <button>See More<img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
          </div>
      </div>
    </div>
  </div>
        
  <div class="container">
    <div class="card">
      <div class="front">
          <div class="box">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/uiux_icon.png">
                <p><?php the_field('snapshot_2_title'); ?></p>
            </div>
        </div>
      <div class="back">
          <div class="box">
            <p><?php the_field('snapshot_2_text'); ?></p>
                              <div class="cta">
                    <a href="/services/ui-ux-design/">
                        <button>See More <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
          </div>
      </div>
    </div>
  </div>

          <div class="container">
    <div class="card">
      <div class="front">
          <div class="box">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/analytics_icon.png">
                <p><?php the_field('snapshot_3_title'); ?></p>
            </div>
        </div>
      <div class="back">
          <div class="box">
            <p><?php the_field('snapshot_3_text'); ?></p>
                              <div class="cta">
                    <a href="/services/seo-marketing/">
                        <button>See More <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
          </div>
      </div>
    </div>
  </div>
</div>

                
<!--
                
                <div class="service_tiles">
                    <div class="tile">
                        <div class="front">
                            <div class="wrapper">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/uiux_icon.png">
                                <p>UI/UX Design & Branding 1</p>
                            </div>
                        </div>
                        <div class="back">
                            <div class="wrapper">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/uiux_icon.png">
                                <p>UI/UX Design & Branding 2</p>
                            </div>
                        </div>
                        
                    </div>
                    <div class="tile">
                        <div class="wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/app_icon.png">
                            <p>Website & App Development</p>
                        </div>
                    </div>
                    <div class="tile">
                        <div class="wrapper">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/analytics_icon.png">
                            <p>Advertising & Search Analytics</p>
                        </div>
                    </div>
                </div>
-->
            </div>
            
        </div>
        
    </section>

<section class="service_flex">
    <div class="flex" id="website">
        <div class="flex_img" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/webDevelopment.jpg)">
            <a href="/services/web-development/">
            <div class="flex_overlay">
                <h5>Let's Develop Together <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
            </div>  
            </a>
        </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h5>Web Development</h5>
                <p><?php the_field('web_development_text'); ?></p>
                <div class="cta">
                    <a href="/services/web-development/">
                        <button>Let's Develop Together <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="flex" id="design">
        <div class="flex_img mobile_only" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/ipadtouch.jpg)">
            <a href="/services/ui-ux-design/">
            <div class="flex_overlay">
                <h5>Designs with Emagid <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
            </div> 
            </a>
        </div> 
        <div class="flex_content">

            <div class="wrapper">
                <h5>UI/UX Design</h5>
                <p><?php the_field('uiux_design_text'); ?></p>
                <div class="cta">
                    <a href="/services/ui-ux-design/">
                        <button>Designs with Emagid <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex_img desktop_only" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/ipadtouch.jpg)">
            <a href="/services/ui-ux-design/">
            <div class="flex_overlay">
                <h5>Designs with Emagid <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>

            </div> 
            </a>
        </div>
    </div>
    
    <div class="flex" id="seo">
        <div class="flex_img" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/seogrowth.jpg)">
            <a href="/services/seo-marketing/">
                <div class="flex_overlay">
                    <h5>Grow with emagid <br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                    
                </div> 
            </a>
        </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h5>SEO & Marketing</h5>
                <p><?php the_field('seo_&_marketing_text'); ?></p>
                <div class="cta">
                    <a href="/services/seo-marketing/">
                        <button>Grow with emagid <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="flex" id="app">
        <div class="flex_img mobile_only" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/unsplash.jpg)">
            <a href="/services/app-development/">
            <div class="flex_overlay">
                    <h5>Go Mobile with Emagid<br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                    
                </div> 
            </a>
        </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h5>App Development</h5>
                <p><?php the_field('app_development_text'); ?></p>
                <div class="cta">
                    <a href="/services/app-development/">
                        <button>Go Mobile with Emagid <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex_img desktop_only" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/unsplash.jpg)">
            <a href="/services/app-development/">
            <div class="flex_overlay">
                    <h5>Go Mobile with Emagid<br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                    
                </div>  
            </a>
        </div>
    </div>
    
        <div class="flex" id="branding">
            
            <div class="flex_img" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/bizcards.jpg)">
                <a href="/services/logo-design-branding/">
                <div class="flex_overlay">
                    <h5>Brand with emagid<br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                    
                </div>
                </a>
            </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h5>Logo Design & Branding</h5>
                <p><?php the_field('logo_design_&_branding_text'); ?></p>
                <div class="cta">
                    <a href="/services/logo-design-branding/">
                        <button class="change_oj">Brand with emagid <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--
    <section class="section_three_home">
        <div class="content">
            <p>EMAGID IS A GLOBAL DIGITAL PRODUCTION FIRM</p>
            <h4 style="text-transform:none;font-size:28px;">We extend the capabilities of businesses and help them scale by producing distinctly better and more innovative technology and design experiences.</h4>
        </div>
        
    </section>
-->

        <div class="hero section" id="section3">
                <section class="section_touch " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png);height:auto;padding-bottom:100px;">
        <div class="content">
            <h4>Get iN TOUCH</h4>
            <div class="cta">
                <a href="/request-a-quote/">
                    <button>Let's Talk <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                </a>
            </div>
        </div>

    </section>

    </div>


<script>
$(".icon_triggers .icon img").click(function(){
    $(".icon_triggers .icon img").removeClass('lightup');
    $(this).addClass('lightup');
});
</script>
                
                
    <script>
                $('.card').click(function(){
  $(this).toggleClass('flipped');
});
                </script>            
                

<?php
get_footer();
