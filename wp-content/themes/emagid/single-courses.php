<?php
get_header();
?>


<section>
    <div class="our_services_section inner_template" >
        <div class="wrapper">
            <section class="section_five_home" id="pricing">
                <div class="content">
                    <div class="package_overview">
                      <h1>  <?php the_title(); ?></h1>
       <?php the_field('overview_content'); ?>
            <?php the_field('detailed_content'); ?>
  
            
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<style>
    .package_overview p > a {
        pointer-events: none;
    }
</style>
<?php
get_footer();
