<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>
        
    <div class="hero section" id="splash" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/business-banner.jpg);">
        <div class="wrapper">
            <a href="/about-self-study/">
            <div class="link_overlay">
            
            </div>
            </a>

        <div class="hero_brand">
            <div class="wrapper">
                <div>
                    <h2 id="brand_trigger" class="active"><?php the_field('banner_title'); ?></h2>

                    <div class="copy">
                        <?php the_field('banner_text'); ?>
                    </div>
                </div>
            </div>
        </div>
      

        </div>
    
</div>



    <section class="section_three_home">
        <h3><?php the_field('intro_header'); ?></h3>
        <div class="content_half">
            <?php the_field('intro_text'); ?>
        </div>
        <div class="content_half">
            <?php the_field('intro_text_2'); ?>
        </div> 
    </section>

<section>
    <div class="our_services_section">
        <div class="wrapper">
        <div class="content">
            <?php the_field('intro_text_3'); ?>
        </div> 
            
        </div>
    </div>

</section>

<?php
get_footer();
