<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>
    <div class="hero section section_default" id="splash">
        <div class="wrapper">
        <div class="hero_brand">
            <div class="wrapper">
        <div class="content" style="width:100%">
            <p><?php the_field('title'); ?></p>
            <h2 style="font-size:34px;">Digital Marketing, <br>Web & App Design, <br>Web & App Development, <br>Search Engine Optimization, <br>And Online Marketing.</h2>
            
        </div>
            </div>
        </div>
            
            <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_min.png" style="top:40%;right:9%;">
                
                <div class="orange_side">
                    
                </div>
            </div>
        </div>
    
</div>


    <section class="section_one_home">
        <div class="content">
            <div class="icon_triggers">
                <div class="icon" id="trigger_software">
                    <img class="lightup" src="<?php echo get_template_directory_uri(); ?>/assets/img/about_icon1o.png">
                </div><div class="icon" id="trigger_seo">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about_icon2o.png">
                </div><div class="icon" id="trigger_marketing">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about_icon3o.png">
                </div>
            </div>
            
            <div class="icon_summary">
                <div class="summary" id="summary_software">
                    <h4><?php the_field('discover_part_1_title'); ?></h4>
                    <p><?php the_field('discover_part_1_text'); ?></p>
                    
                    <a href="<?php the_field('discover_part_1_link'); ?>">
                        <p class="discover">Discover<br>More </p>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png">
                    </a>
                    
                </div>
                <div class="summary" id="summary_seo" style="display:none;">
                    <h4><?php the_field('discover_part_2_title'); ?></h4>
                    <p><?php the_field('discover_part_2_text'); ?></p>
                    
                    <a href="<?php the_field('discover_part_2_link'); ?>">
                        <p class="discover">Discover<br>More </p>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png">
                    </a>
                    
                </div>
                <div class="summary" id="summary_marketing" style="display:none;">
                    <h4><?php the_field('discover_part_3_title'); ?></h4>
                    <p><?php the_field('discover_part_3_text'); ?></p>
                    
                    <a href="<?php the_field('discover_part_3_link'); ?>">
                        <p class="discover">Discover<br>More </p>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png">
                    </a>
                    
                </div>
            </div>
        </div>

    </section>

    <section class="section_two_home" id="capabilities">
        <div class="section_header">
            <h2>Our Capabilities</h2>
        </div>

        <div class="service_list">
            <div class="list column_2">

                <ul>
                    <li><h3>App & Web</h3></li>
                    <div class="two_columns" style="column-count: 2;">
                        <li><p>Web Design</p></li>
                        <li><p>Custom Web Development</p></li>
                        <li><p>App Design</p></li>
                        <li><p>App Development</p></li>
                        <li><p>Interaction Design</p></li>
                        <li><p>Responsive Design</p></li>
                        <li><p>Content Management Systems</p></li>
                        <li><p>E-Commerce Systems</p></li>
                        <li><p>Enterprise Solutions</p></li>
                        <li><p>Dynamic AngularJS Solutions</p></li>
                        <li><p>PHP Development</p></li>
                    </div>
                </ul>
            </div><div class="list">
            <ul>
                <li><h3>SEO & Marketing</h3></li>
                <li><p>Search Engine Optimization</p></li>
                <li><p>Pay-Per-Click Management</p></li>
                <li><p>Facebook Ad Management</p></li>
                <li><p>Online Marketing</p></li>
                <li><p>Email Marketing</p></li>
            </ul>
            </div><div class="list">

                <ul>
                    <li><h3>Branding</h3></li>
                    <li><p>Logo Design</p></li>
                    <li><p>Branding</p></li>
                    <li><p>Social Media</p></li>
                </ul>
            </div>
        </div>

    </section>

    <section class="section_three_home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/skyline.png)">
        <div class="content">
            <p><?php the_field('mission_statement_title'); ?></p>
            <h4><?php the_field('mission_statement_text'); ?></h4>
        </div>
        
    </section>

    <section class="section_four_home" id="technology">
        <div class="content">
            <h4>Some of the Technologies Supporting our design & development</h4>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_ps.jpg">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_ai.png">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_css.png">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_html.png">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_jquery.png">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_wordpress.png">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tech_android.png">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ios_icon.png">
        </div>
        
    </section>

    <section class="section_five_home" id="press">
        <div class="content">
            <h4>press and media</h4>
            <p>Our story and vision, at your fingertips</p>
            
<!--            <div class="media_slider">-->
                       <?php
	  			$args = array(
	    		'post_type' => 'press'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
            <div class="media_box">
                <div class="header">
                    <h5><?php the_field('title'); ?> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></h5>
                </div>
                <div class="press">
                    <p><?php the_field('content'); ?></p>
                </div>
            
            </div>
                   <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?> 

<!--                </div>-->
        </div>
        
        
    </section>

    <section class="section_six_home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/office.png)" id="careers">
        <div class="content">
            <h4>Join The Emagid Team</h4>
            <h5>Developers & Designers</h5>
            <p>At eMagid, we are always on the lookout for creative and experienced individuals to join our team. Job opportunities are available for developers and designers to work in our New York offices. Skills in high demand for job openings include:</p>
            
            <ul class="job_list">
                <li class="active"><p>PHP Development</p></li>
                <li><p>UI/UX Design</p></li>
                <li><p>Web & App Design</p></li>
                <li><p>Illustration & Graphic</p></li>
                <li><p>AngularJS</p></li>
                <li><p>Objective-C & Swift Development</p></li>
                <li><p>Android Development</p></li>
                <li><p>Interactive & Keyframe Design</p></li>
            </ul>
        </div>
        <div class="form_container">
            <div class="form_apply">
                <p>Interested applicants please send your resume to <strong>info@emagid.com</strong> or fill out the form below.</p>
                
                <?php echo do_shortcode('[contact-form-7 id="7" title="Job Application Form"]'); ?>
                
            </div>
        </div>
    </section>


<script>
$(".icon_triggers .icon img").click(function(){
    $(".icon_triggers .icon img").removeClass('lightup');
    $(this).addClass('lightup');
});
    
$(".icon#trigger_software").click(function(){
    $(".summary").hide();
    $(".summary#summary_software").show();
});

$(".icon#trigger_seo").click(function(){
    $(".summary").hide();
    $(".summary#summary_seo").show();
});

$(".icon#trigger_marketing").click(function(){
    $(".summary").hide();
    $(".summary#summary_marketing").show();
});
    
$("ul.job_list li").click(function(){
    $("ul.job_list li").removeClass("active");
    $(this).addClass("active");
});
    
    
$('.media_slider').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  centerMode: true,
  variableWidth: true
});
</script>
<?php
get_footer();
