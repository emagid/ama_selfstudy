<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <!--    Pagepiling-->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/libs/pagepiling/jquery.pagepiling.css" rel="stylesheet">
    <script
  src="<?php echo get_template_directory_uri(); ?>/assets/libs/pagepiling/jquery.pagepiling.min.js"></script>

	<script type="text/javascript">
		var deleteLog = false;

		$(document).ready(function() {
	    	$('#pagepiling').pagepiling({
	    		menu: '#menu',
			    sectionsColor: ['#bfda00', '#2ebe21', '#2C3E50', '#51bec4'],
			    onLeave: function(index, nextIndex, direction){
			    	if(deleteLog){
			    		$('#callbacksDiv').html('');
			    	}
			    },
			    afterRender: function(){
			    },
			    afterLoad: function(anchorLink, index){

			    	//section 2
					if(index == 2){
						//moving the image
						$('#section2').find('.intro').delay(100).animate({
							left: '0%'
						}, 1500, 'easeOutExpo', function(){
							$('#section2').find('p').first().fadeIn(700, function(){
								$('#section2').find('p').last().fadeIn(600);
							});
						});


					}

					//section 3
					if(index == 3){
						//section 3
						$('#section3').find('.intro').delay(100).animate({
								left: '0%'
						}, 1500, 'easeOutExpo');
					}

					deleteLog = true;
			    }
			});
	    });
    </script>

    <style>
    #section2 .intro{
    	left: -150%;
		position: relative;
    }
    #section2 p{
    	display: none;
    }
    #section3 .intro{
    	left: 140%;
		position: relative;
    }
    </style>



	<div id="pagepiling">
        
	    <div class="hero section" id="section1" style="background-image:url(http://emagid.test/wp-content/uploads/2018/06/XI5A6461.jpg);">
	    	<div class="intro">
	    		        <div class="hero_side" style="background:<?php the_field('project_color'); ?>;">
            <div class="wrapper intro">
                <h3><?php the_field('project'); ?></h3>
                <p><?php the_field('tagline'); ?></p>
                
                <div class="cta">
                    <a href="<?php the_permalink(); ?>">
                        <button>View Case Study <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
	    	</div>
	    </div>
	    <div class="section hero" id="section2" style="background-image:url(http://emagid.test/wp-content/uploads/2018/06/XI5A6461.jpg);">
	    	<div class="intro">
	    		        <div class="hero_side" style="background:<?php the_field('project_color'); ?>;">
            <div class="wrapper intro">
                <h3><?php the_field('project'); ?></h3>
                <p><?php the_field('tagline'); ?></p>
                
                <div class="cta">
                    <a href="<?php the_permalink(); ?>">
                        <button>View Case Study <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
	    	</div>
	    </div>
        	    <div class="section" id="section3">
	    	<div class="intro">
	    		        <div class="hero_side" style="background:<?php the_field('project_color'); ?>;">
            <div class="wrapper intro">
                <h3><?php the_field('project'); ?></h3>
                <p><?php the_field('tagline'); ?></p>
                
                <div class="cta">
                    <a href="<?php the_permalink(); ?>">
                        <button>View Case Study <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
	    	</div>
	    </div>
	    <div class="section" id="section4">
	    	<div class="intro">
	    		        <div class="hero_side" style="background:<?php the_field('project_color'); ?>;">
            <div class="wrapper intro">
                <h3><?php the_field('project'); ?></h3>
                <p><?php the_field('tagline'); ?></p>
                
                <div class="cta">
                    <a href="<?php the_permalink(); ?>">
                        <button>View Case Study <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
	    	</div>
	    </div>
	</div>
<?php
get_footer();
