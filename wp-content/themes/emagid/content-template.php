<?php
/**
 * Template Name: Content Template
 *
 */
get_header();
?>


<section>
    <div class="our_services_section inner_template" >
        <div class="wrapper">
            <section class="section_five_home" id="pricing">
                <div class="content">
                    <div class="package_overview">
                        <?php the_field('content'); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<?php
get_footer();
