<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>

    <div class="hero section section_default" id="splash">
        <div class="wrapper">
        <div class="hero_brand">
            <div class="wrapper">
        <div class="content">
            <p><?php the_title(); ?></p>
            <h2 style="font-size:36px;">We recommend using the following affiliates. They offer the best value and quality in their respective fields.</h2>
            
        </div>
            </div>
        </div>
            
            <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_min.png" style="top:40%;right:9%;">
                
                <div class="orange_side">
                    
                </div>
            </div>
        </div>
    
</div>

<section class="service_flex">
    <div class="flex" id="website">
        <div class="flex_img" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/sabres.png);background-size:auto;background-color:#89C840;"> 
        </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h6>Wordpress Security Monitoring</h6>
                <h5>Sabres Security</h5>
                
                <p>SABRES is committed to bring to market the most robust SaaS protection for open source CMS web and mobile applications.</p>
                <div class="cta">
                    <a href="https://www.sabressecurity.com/" target="_blank">
                        <button>Visit Site <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="flex" id="design">
        <div class="flex_img mobile_only" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/payeezy.png);background-size:50%;background-color:#F7C020;"></div>
        <div class="flex_content">

            <div class="wrapper">
                <h6>Scalable E-Commerce Solutions</h6>
                <h5>Clover Gateway</h5>
                
                <p>Build your business with Payeezy, the simplest way to accept transactions online.</p>
                <div class="cta">
                    <a href="https://www.payeezy.com/content/payeezy/www/en_us/index.html" target="_blank">
                        <button>Visit Site <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex_img desktop_only" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/payeezy.png);background-size:50%;background-color:#F7C020;"> 
        </div>
    </div>
    
    <div class="flex" id="seo">
        <div class="flex_img" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/pier.png);background-size:40%;background-color:#39A5C8;"> 
        </div>
        <div class="flex_content">
            
            <div class="wrapper">
                <h6>The Ultimate NYC Event Space</h6>
                <h5>Pier36</h5>
                <p>Basketball City & Pier 36 host 700,000 visitors per year & 1,500 top corporations with their year-round programming.</p>
                <div class="cta">
                    <a href="http://pier36nyc.com/" target="_blank">
                        <button>Visit Site <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_orange.png" alt="arrow_cta"></button>
                    </a>
                </div>
            </div>
        </div>
    </div>

</section>

<!--
    <section class="section_three_home">
        <div class="content">
            <p>EMAGID IS A GLOBAL DIGITAL PRODUCTION FIRM</p>
            <h4 style="text-transform:none;font-size:28px;">We extend the capabilities of businesses and help them scale by producing distinctly better and more innovative technology and design experiences.</h4>
        </div>
        
    </section>
-->

        <div class="hero section" id="section3">
                <section class="section_touch " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png);height:auto;padding-bottom:100px;">
        <div class="content">
            <h4>Get iN TOUCH</h4>
            <div class="cta">
                <a href="/request-a-quote/">
                    <button>Let's Talk <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow_white.png" alt="arrow_cta"></button>
                </a>
            </div>
        </div>

    </section>

    </div>


<script>
$(".icon_triggers .icon img").click(function(){
    $(".icon_triggers .icon img").removeClass('lightup');
    $(this).addClass('lightup');
});
</script>
                
                
    <script>
                $('.card').click(function(){
  $(this).toggleClass('flipped');
});
                </script>            
                

<?php
get_footer();
