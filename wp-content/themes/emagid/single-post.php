<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header();
?>
    <section class="section_default blog_hero" style="background-image:url(<?php the_field('hero_image'); ?>)">
        <div class="blog_overlay">
            <div class="content">
                <p></p>
                <h2><?php the_title(); ?></h2>

            </div>
        </div>
        
    </section>

<section class="default_content">
    <div class="single_post_wrapper">
        <?php the_content(); ?>
        
        <div class="newsletter_box">
            
        </div>
    </div>
        
</section>


<?php
get_footer();
