<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	</div><!-- .site_content-->

	<footer class="site_footer" id="desktop">

        <div class="wrapper">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'menu-2'
            ) );
            ?>
            
        </div>
                <div class="bottom_bar">
            <p>© 2009 American Management Association</p>
        </div>
        
	</footer>

</div><!-- .site_holder -->
<!--
<script>
    
    
$(window).load(function(){
   $(".hero_brand").show("slide", {
      direction: "up"
   }, 1500);
    $(".two_sided").show("slide", {
      direction: "right"
   }, 2000);
    
    
});
</script>
-->

<?php wp_footer(); ?>

</body>
</html>
