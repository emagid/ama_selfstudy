<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

    <ul>
        <li class="home">
            <a href="<?php echo get_permalink( $post->post_parent ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/house.png">
                <p>Home</p>
            </a>

        </li>
        <!--
        <li>
            <a href="<?php echo get_permalink( $post->post_parent ); ?>#gallery">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery.png">
                <p>Gallery</p>
            </a>
        </li>
-->
        <li class="maps">
            <a href="<?php echo get_permalink( $post->post_parent ); ?>directions-maps/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/map.png">
                <p>Directions</p>
            </a>
        </li>
        <li class="floor_plans">
            <a href="<?php echo get_permalink( $post->post_parent ); ?>floor-plans/">
                <img  src="<?php echo get_template_directory_uri(); ?>/assets/img/plotter.png">
                <p>Floor Plans</p>
            </a>
        </li> 
        <li class="packages">
            <a href="<?php echo get_permalink( $post->post_parent ); ?>meeting-packages/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/package.png">
                <p>Meeting Packages</p>
            </a>
        </li>


        <li class="hotels">
            <a href="<?php echo get_permalink( $post->post_parent ); ?>hotels/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hotel.png">
                <p>Hotels</p>
            </a>
        </li>
        <li class="menu">
            <a href="<?php echo get_permalink( $post->post_parent ); ?>menus/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/menu.png">
                <p>Menus</p>
            </a>
        </li>


    <li class="attractions">
        <a href="<?php echo get_permalink( $post->post_parent ); ?>explore-area/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/explore.png">
            <p>Explore Area</p>
        </a>
    </li>
    <li class="book_now" id="ny_book">
        <a href="/book-now/step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
    <li class="book_now" id="atl_book">
        <a href="/book-now/atl-step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
    <li class="book_now" id="sf_book">
        <a href="/book-now/sf-step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
    <li class="book_now" id="dc_book">
        <a href="/book-now/dc-step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>

    </ul>