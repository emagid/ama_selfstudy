<?php
/**
 * Template Name: Inner Template
 *
 */
get_header();
?>
  <section class="inner_template">     
    <div class="hero section" id="splash" style="background-image:url(<?php the_field('banner'); ?>);">
        <div class="wrapper">
                        <div class="two_sided">
                <div class="grey_side" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/grey_banner.png);">
                
                    <div class="inner_hero_content">
                        <div class="wrapper">
                            <h2 class="active"><?php the_title(); ?></h2>

                            <div class="copy">
                                <p><?php the_field('address'); ?></p>
                            <div class="cta" id="ny_book">
                                <a href="/book-now/step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            <div class="cta" id="atl_book">
                                <a href="/book-now/atl-step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            <div class="cta" id="sf_book">
                                <a href="/book-now/sf-step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            <div class="cta" id="dc_book">
                                <a href="/book-now/dc-step-1/">
                                    <button>Book Now</button>
                                </a>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
      

        </div>
    
</div>
<div class="nav_bar" id="sticker">
<ul>
    <li>
        <a href="#overview" class="scroll">
            <img class="active" src="<?php echo get_template_directory_uri(); ?>/assets/img/house.png">
            <p>Home</p>
        </a>
    </li>
<!--
    <li>
        <a href="#gallery" class="scroll">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gallery.png">
            <p>Gallery</p>
        </a>
    </li>
-->
        <li>
        <a href="<?php echo get_permalink( $post->post_parent ); ?>directions-maps/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/map.png">
            <p>Directions</p>
        </a>
    </li>
        <li>
        <a href="<?php echo get_permalink( $post->post_parent ); ?>floor-plans/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/plotter.png">
            <p>Floor Plans</p>
        </a>
    </li>  
    <li>
        <a href="<?php echo get_permalink( $post->post_parent ); ?>meeting-packages/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/package.png">
            <p>Meeting Packages</p>
        </a>
    </li>
    

   
    <li>
        <a href="<?php echo get_permalink( $post->post_parent ); ?>hotels/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hotel.png">
            <p>Hotels</p>
        </a>
    </li>
    <li>
        <a href="<?php echo get_permalink( $post->post_parent ); ?>menus/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/menu.png">
            <p>Menus</p>
        </a>
    </li>

    <li>
        <a href="<?php echo get_permalink( $post->post_parent ); ?>explore-area/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/explore.png">
            <p>Explore Area</p>
        </a>
    </li>
    
    <li class="book_now" id="ny_book">
        <a href="/book-now/step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
    <li class="book_now" id="atl_book">
        <a href="/book-now/atl-step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
    <li class="book_now" id="sf_book">
        <a href="/book-now/sf-step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
    <li class="book_now" id="dc_book">
        <a href="/book-now/dc-step-1/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/verified.png">
            <p>Book Now</p>
        </a>
    </li>
    
</ul>
</div>


    <section class="section_three_home" id="overview">
        <div class="content">
            <h2><?php the_field('overview_title'); ?></h2>
            <?php the_field('overview'); ?>
        </div>
        
    </section>
    
    <div class="our_services_section pricing" id="rates">
        <div class="wrapper" style="width:100%;">
            <section class="section_five_home" id="pricing">
                <div class="content">
                    <div class="package_overview">
                        <?php the_field('gallery'); ?>
                    </div>            
                </div>
            </section>
        </div>
    </div>
      
<section>
    <div class="our_services_section pricing" id="rates">
        <div class="wrapper">
            <h2>Meeting Room Package Prices</h2>
            
            <section class="section_five_home" id="pricing">
            <div class="content">
                <div class="package_overview">
                    <?php the_field('packages'); ?>
                </div>

                

                            <div class="cta">
                                <a href="<?php echo get_permalink( $post->post_parent ); ?>meeting-packages/">
                                    <button>View All Packages</button>
                                </a>
                            </div>
            
        </div>

                </section>
    </div>
    </div>
    
<section>
    <div class="our_services_section pricing contacts" id="rates">
        <div class="wrapper">
<h2>Contacts</h2>
            
            <section class="section_five_home" id="hotels">
            <div class="content">

                                  <?php
//                Washington DC
                if( is_page( 324 ) ) {
        $args = array(
        'post_type' => 'contacts',
            'cat' => '10'
            
        );
                    } 
//                San Francisco
                else if( is_page( 326 ) ) {
                    
                $args = array(
        'post_type' => 'contacts',
                    'cat' => '9'
        ); 
                }
//                Atlanta
                else if( is_page( 320 ) ) {
                    
                $args = array(
        'post_type' => 'contacts',
                    'cat' => '8'
        ); 
                }
//                New York
                else {
                    
                $args = array(
        'post_type' => 'contacts',
                    'cat' => '7'
        ); 
                }
                
                
        $products = new WP_Query( $args );
            if( $products->have_posts() ) {
            while( $products->have_posts() ) {
        $products->the_post();
    ?> 
                <div class="media_box">

                    <div class="header contact_header">
                        <div class="wrapper">
                            <h5><?php the_field('name'); ?></h5>
                            <p><?php the_field('contact_info'); ?></p>
                        </div>
                    </div>     
                </div>
    <?php
        }
            }
        else {
        echo 'No Contacts Found';
        }
    ?> 
         
                


            
        </div>
                </section>
    </div>
    </div>
</section>
</section>

<!--
    <div class="hero section" id="section3">
    <section class="section_touch " style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/work.png);">
        <div class="content">
            <h6>Want to Book A Room</h6>
            <p>Reach Out, Let's Chat</p>
            <div class="cta">
                <a href="/contact">
                    <button>Contact</button>
                </a>
            </div>
        </div>

    </section>

    </div>
-->
</section> 


<style>
    .site_holder header {
        position: relative;
    }
    .site_content {
        padding-top: 0
    }
</style>

<script>
$(document).ready(function(){

    // Page opening animations
    setTimeout(function(){
        $('.grey_side').css('width', '65%');
    }, 500);

    setTimeout(function(){
        $('.inner_hero_content').fadeIn();
    }, 1000);

    $(".nav_bar").find("a.scroll").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });
    
    $("#sticker").sticky({topSpacing:0});
    
    
});
</script>

<?php
get_footer();
