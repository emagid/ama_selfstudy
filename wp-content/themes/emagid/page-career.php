<?php
get_header();
?>


<section>
    <div class="our_services_section inner_template" >
        <div class="wrapper">
            <section class="section_five_home" id="pricing">
                <div class="content">
                    <div class="package_overview">
                                                                 <?php
            $args = array(
      'post_type' => 'courses',
                'cat' => '15'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
       <?php the_title(); ?>
            <?php the_field('overview_content'); ?>
        <a href="<?php the_permalink(); ?>">
           <p>View More</p>
       </a>     
            
                <?php
            }
                  }
            else  {
            echo 'No Events Found';
            }
      ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<style>
    .package_overview p > a {
        pointer-events: none;
    }
</style>
<?php
get_footer();
